# BusinessCardOcr

Business Card OCR is a Console Line Application that takes inputs from a user and extracts the information for a name, phone number, and email address.

## Running the Application

Before beginning, you will need to download add Java to windows if you have not already. The latest version can be found here.

https://www.java.com/en/download/

Once installed, you will need to download the zip file and extract it to a folder of your choosing.
Once extracted, open the console in the location that contains the BusinessCardOcr-1.0-SNAPSHOT.jar file. 
To run the .jar file, you will want to type...
```
java -jar BusinessCardOcr-1.0-SNAPSHOT.jar 
```
into the console. 

## Testing the Application

The Business Card OCR will begin to run and it will prompt you to input your data for parsing or if you wish to exit. The application does accept both

```
ASYMMETRIK LTD
Mike Smith
Senior Software Engineer
(410)555-1234
msmith@asymmetrik.com
```

```
ASYMMETRIK LTD Mike Smith Senior Software Engineer (410)555-1234 msmith@asymmetrik.com
```

It does not accept multiple Examples at a time so attempts such as 

```
ASYMMETRIK LTD
Mike Smith
Senior Software Engineer
(410)555-1234
msmith@asymmetrik.com
Foobar Technologies
Analytic Developer
Lisa Haung
1234 Sentry Road
Columbia, MD 12345
Phone: 410-555-1234
Fax: 410-555-4321
lisa.haung@foobartech.com
```
will end up returning on the first matches.

The application will continue to run until you wish to exit by typing ```Exit``` into the console.