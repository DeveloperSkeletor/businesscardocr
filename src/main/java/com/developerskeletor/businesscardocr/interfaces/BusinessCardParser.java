package com.developerskeletor.businesscardocr.interfaces;

/**
 * Interface that defines a basic Business Card Parser
 */
public interface BusinessCardParser {

    /**
     * Parses a String Document for information to fill out
     * a new contact information.
     * @param document the document that is received from the
     *                 optical character recognition (OCR)
     *                 component.
     * @return the parsed contact information.
     */
    ContactInfo getContactInfo(String document);
}
