package com.developerskeletor.businesscardocr.interfaces;

/**
 * Interface class defining the necessary parameters needed for basic
 * contact information acquired from a business card.
 */
public interface ContactInfo {

    /**
     * @return the name of the contact.
     */
    String getName();

    /**
     * @return the phone number of the contact.
     */
    String getPhoneNumber();

    /**
     * @return the email address of the contact.
     */
    String getEmailAddress();
}
