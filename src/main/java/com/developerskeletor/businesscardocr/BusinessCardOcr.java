package com.developerskeletor.businesscardocr;

import com.developerskeletor.businesscardocr.interfaces.ContactInfo;
import com.developerskeletor.businesscardocr.models.BusinessCardParserImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BusinessCardOcr {

    public static void main(String[] args){
        boolean isRunning = true;
        do{
            System.out.println("Please input the business card example that you would like to test and press Enter.");
            System.out.println("If you wish to quit the application, just type 'Exit'.");
            Scanner input = new Scanner(System.in);
            List<String> lines = new ArrayList<>();
            String lineNew;
            while (input.hasNextLine()) {
                lineNew = input.nextLine();
                if (lineNew.isEmpty()) {
                    break;
                }else if (lineNew.equals("Exit")){
                    return;
                }
                lines.add(lineNew);
            }
            String businessCardInput = String.join(" ", lines);
            BusinessCardParserImpl parser = new BusinessCardParserImpl();
            ContactInfo contactInfo = parser.getContactInfo(businessCardInput);
            System.out.println();
            System.out.println("Name: " + contactInfo.getName());
            System.out.println("Phone: " + contactInfo.getPhoneNumber());
            System.out.println("Email: " + contactInfo.getEmailAddress());
            System.out.println();
        }while (isRunning);
    }
}
