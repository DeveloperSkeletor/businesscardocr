package com.developerskeletor.businesscardocr.models;

import com.developerskeletor.businesscardocr.interfaces.*;

public class ContactInfoImpl implements ContactInfo {

    private String name;
    private String phoneNumber;
    private String emailAddress;

    //I have added two CTORs in this implementation because based on what this could be used for, I think
    //it would make sense for someone to be able to create an empty contact info as well as a filled one.

    public ContactInfoImpl(){
        this.name = "";
        this.phoneNumber = "";
        this.emailAddress = "";
    }

    public ContactInfoImpl(String name, String phoneNumber, String emailAddress){
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }

    @Override
    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    @Override
    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    //I added a version that accepts numbers for varieties sake.
    public void setPhoneNumber(int phoneNumber){
        this.phoneNumber = String.valueOf(phoneNumber);
    }

    @Override
    public String getEmailAddress() {
        return this.emailAddress;
    }

    public void setEmailAddress(String emailAddress){
        this.emailAddress = emailAddress;
    }
}
