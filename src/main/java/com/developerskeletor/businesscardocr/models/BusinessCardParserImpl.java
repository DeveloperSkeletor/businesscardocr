package com.developerskeletor.businesscardocr.models;

import com.developerskeletor.businesscardocr.interfaces.*;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;


public class BusinessCardParserImpl implements BusinessCardParser {

    private static final Logger LOGGER = Logger.getLogger(BusinessCardParserImpl.class.getName());
    private static final String PHONE_REGEX = "(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}";
    private static final String EMAIL_REGEX = "[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+";
    private StanfordCoreNLP pipeline;
    private ContactInfoImpl contactInfo;

    //Standford provides a Simple Version of their CoreNLP but I found that with what I was doing
    //it made more sense to set the properties directly.
    public BusinessCardParserImpl(){
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner");
        props.setProperty("ner.model", "edu/stanford/nlp/models/ner/english.all.3class.distsim.crf.ser.gz");
        this.pipeline = new StanfordCoreNLP(props);
        this.contactInfo = new ContactInfoImpl();
    }

    @Override
    public ContactInfo getContactInfo(String document) {
        String name = parseForName(document);
        String phoneNumber = parseForPhoneNumber(document);
        String emailAddress = parseForEmailAddress(document);
        setContactInfo(name, phoneNumber, emailAddress);
        return this.contactInfo;
    }

    //Most of these methods will either pass back an empty String or the results that were found.
    //I didn't want to pass back nulls as that can lead to exceptions and would rather log the issue
    //and return something that is expected in some fashion.

    //Parsing for the name was the trickiest of the problems that I faced with this application.
    //I have never used Name Entity Recognition before so it was an interesting challenge. I did not have
    //a reason for picking Standford's over apaches beyond that it was the first one that I found when
    //I first began searching for a solution to this type of problem.
    private String parseForName(String document){
        if (document == null || document.isEmpty()){
            LOGGER.log(Level.WARNING, "The document provided was null or empty.");
            return "";
        }
        Annotation annotatedDocument = new Annotation(document);
        String fullName = ExtractNameEntity(annotatedDocument);
        if (fullName.equals("")){
            LOGGER.log(Level.WARNING, "The document provided could not parse a name.");
            return "";
        }
        return fullName;
    }

    //I separated out the extraction for the parseForName function as I try and follow the
    //Single Responsibility Principle for code cleanliness.
    private String ExtractNameEntity(Annotation annotatedDocument){
        pipeline.annotate(annotatedDocument);
        List<CoreMap> sentences = annotatedDocument.get(CoreAnnotations.SentencesAnnotation.class);
        List<String> names = new ArrayList<String>();
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
                if (ne.equals("PERSON")) {
                    names.add(word);
                }
            }
        }
        if (!names.isEmpty()){
            return String.join(" ", names);
        }
        else{
            LOGGER.log(Level.WARNING, "A person's name was not found.");
            return "";
        }
    }

    //Both the parseForPhoneNumber and the parseForEmailAddress utilize regex patterns to hunt down
    //the necessary information. The only major difference between the two is that the parseForEmailAddress
    //function takes case sensitivity into account.
    private String parseForPhoneNumber(String document){
        if (document == null || document.isEmpty()){
            LOGGER.log(Level.WARNING, "The document provided was null or empty.");
            return "";
        }
        Pattern phonePattern = Pattern.compile(PHONE_REGEX);
        Matcher phoneMatcher = phonePattern.matcher(document);
        if (phoneMatcher.find()){
            return phoneMatcher.group().replaceAll("[^\\d]", "");
        }
        LOGGER.log(Level.WARNING, "The document provided could not parse a phone number.");
        return "";
    }

    private String parseForEmailAddress(String document){
        if (document == null || document.isEmpty()){
            LOGGER.log(Level.WARNING, "The document provided was null or empty.");
            return "";
        }
        Pattern emailPattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
        Matcher emailMatcher = emailPattern.matcher(document);
        if (emailMatcher.find()){
            return emailMatcher.group();
        }
        LOGGER.log(Level.WARNING, "The document provided could not parse an email address.");
        return "";
    }

    private void setContactInfo(String name, String phoneNumber, String emailAddress){
        this.contactInfo.setName(name);
        this.contactInfo.setPhoneNumber(phoneNumber);
        this.contactInfo.setEmailAddress(emailAddress);
    }
}
