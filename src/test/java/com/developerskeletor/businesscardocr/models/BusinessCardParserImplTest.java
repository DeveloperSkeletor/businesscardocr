package com.developerskeletor.businesscardocr.models;

import com.developerskeletor.businesscardocr.interfaces.ContactInfo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BusinessCardParserImplTest {

    private static final String testDocument1 = "ASYMMETRIK LTD\n" +
            "Mike Smith\n" +
            "Senior Software Engineer\n" +
            "(410)555-1234\n" +
            "msmith@asymmetrik.com";

    private static final String testDocument2 = "Foobar Technologies\n" +
            "Analytic Developer\n" +
            "Lisa Haung\n" +
            "1234 Sentry Road\n" +
            "Columbia, MD 12345\n" +
            "Phone: 410-555-1234\n" +
            "Fax: 410-555-4321\n" +
            "lisa.haung@foobartech.com";

    private static final String testDocument3 ="Arthur Wilson\n" +
            "Software Engineer\n" +
            "Decision & Security Technologies\n" +
            "ABC Technologies\n" +
            "123 North 11th Street\n" +
            "Suite 229\n" +
            "Arlington, VA 22209\n" +
            "Tel: +1 (703) 555-1259\n" +
            "Fax: +1 (703) 555-1200\n" +
            "awilson@abctech.com";

    @Test
    void getContactInfo_TestDocument1() {
        BusinessCardParserImpl businessCardParser = new BusinessCardParserImpl();
        ContactInfo contactInfo = businessCardParser.getContactInfo(testDocument1);
        assertEquals("Mike Smith", contactInfo.getName());
        assertEquals("4105551234", contactInfo.getPhoneNumber());
        assertEquals("msmith@asymmetrik.com", contactInfo.getEmailAddress());
    }

    @Test
    void getContactInfo_TestDocument2() {
        BusinessCardParserImpl businessCardParser = new BusinessCardParserImpl();
        ContactInfo contactInfo = businessCardParser.getContactInfo(testDocument2);
        assertEquals("Lisa Haung", contactInfo.getName());
        assertEquals("4105551234", contactInfo.getPhoneNumber());
        assertEquals("lisa.haung@foobartech.com", contactInfo.getEmailAddress());
    }

    @Test
    void getContactInfo_TestDocument3() {
        BusinessCardParserImpl businessCardParser = new BusinessCardParserImpl();
        ContactInfo contactInfo = businessCardParser.getContactInfo(testDocument3);
        assertEquals("Arthur Wilson", contactInfo.getName());
        assertEquals("17035551259", contactInfo.getPhoneNumber());
        assertEquals("awilson@abctech.com", contactInfo.getEmailAddress());
    }

    @Test
    void getContactInfo_TestNull() {
        BusinessCardParserImpl businessCardParser = new BusinessCardParserImpl();
        ContactInfo contactInfo = businessCardParser.getContactInfo(null);
        assertEquals("", contactInfo.getName());
        assertEquals("", contactInfo.getPhoneNumber());
        assertEquals("", contactInfo.getEmailAddress());
    }
}