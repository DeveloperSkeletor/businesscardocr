package com.developerskeletor.businesscardocr.models;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class ContactInfoImplTest {

    private static final String name = "Jenny Doe";
    private static final String phoneNumber = "867-5309";
    private static final String emailAddress = "JennyDoe@gmail.com";

    private ContactInfoImpl contactInfo;

    @BeforeEach
    void setUp() {
        contactInfo = new ContactInfoImpl(name, phoneNumber,emailAddress);
    }

    @Test
    void getName() {
        assertEquals(name, contactInfo.getName());
    }

    @Test
    void setName() {
        String newName = "Jane Smith";
        contactInfo.setName(newName);
        assertEquals(newName, contactInfo.getName());
    }

    @Test
    void getPhoneNumber() {
        assertEquals(phoneNumber, contactInfo.getPhoneNumber());
    }

    @Test
    void setPhoneNumber() {
        String newPhoneNumber = "123-4567";
        contactInfo.setPhoneNumber(newPhoneNumber);
        assertEquals(newPhoneNumber, contactInfo.getPhoneNumber());
    }

    @Test
    void setPhoneNumber1() {
        int newPhoneNumber = 1234567;
        contactInfo.setPhoneNumber(newPhoneNumber);
        assertEquals(String.valueOf(newPhoneNumber), contactInfo.getPhoneNumber());
    }

    @Test
    void getEmailAddress() {
        assertEquals(emailAddress, contactInfo.getEmailAddress());
    }

    @Test
    void setEmailAddress() {
        String newEmailAddress = "JennySmith@hotmail.com";
        contactInfo.setEmailAddress(newEmailAddress);
        assertEquals(newEmailAddress, contactInfo.getEmailAddress());
    }
}